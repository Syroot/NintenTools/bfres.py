from setuptools import setup

setup(name="bfres",
      packages=["bfres"],
      version="0.1.0",
      python_requires=">=3",
      install_requires=["binaryio"],
      description="Python package for loading and saving the Nintendo BFRES graphics archive format.",
      keywords="bfres nintendo nintendo-ware wii-u",
      author="Syroot",
      author_email="dev@syroot.com",
      license="MIT",
      url="https://github.com/Syroot/bfres",
      download_url="https://github.com/Syroot/bfres/archive/0.1.0.tar.gz")
