# bfres
Python module for loading and saving the Nintendo BFRES graphics archive format.

# PyPI
The package can be found on [PyPI](https://pypi.org/project/bfres/).
